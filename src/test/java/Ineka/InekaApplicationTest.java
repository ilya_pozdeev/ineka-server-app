package Ineka;

import Ineka.Model.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class InekaApplicationTest {

	@Autowired
	UserRepository userRepository;

	static String newEmail;
	static User savedUser;
	static User userToSave;

	@Before
	public void setup() {
		newEmail = RandomStringUtils.randomAlphabetic(10) + "@gmail.com";
	}

	@Test
	public void test01SaveNewUser() {
		userToSave = new User("Test", "Testikus", newEmail, "test123");
		savedUser = userRepository.save(userToSave);
		assertNotNull(savedUser, "User was not saved!");
		assertEquals(savedUser.getName(), userToSave.getName(), "User name was not saved!");
	}

	@Test
	public void test02FindNewUserByNameSurname() {
		List<User> result = userRepository.findByNameSurname(userToSave.getName(), userToSave.getSurname());
		assertTrue(result.size() > 0);
	}

	@Test
	public void test03DeleteNewUser() {
		userRepository.delete(savedUser);
	}
}
