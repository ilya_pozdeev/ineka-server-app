package Ineka.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Chat {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String chat_name;

    private Integer id_sender;

    private Integer id_recipient;

    public Chat() {}

    public Chat(String chat_name, Integer id_sender, Integer id_recipient) {
        this.id = null;
        this.chat_name = chat_name;
        this.id_sender = id_sender;
        this.id_recipient = id_recipient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer idChat) {
        this.id = idChat;
    }

    public String getChatName() {
        return chat_name;
    }

    public void setChatName(String chat_name) {
        this.chat_name = chat_name;
    }

    public Integer getIdSender() {
        return id_sender;
    }

    public void setIdSender(Integer id_sender) {
        this.id_sender = id_sender;
    }

    public Integer getIdRecipient() {
        return id_recipient;
    }

    public void setIdRecipient(Integer id_recipient) {
        this.id_recipient = id_recipient;
    }

    @Override
    public String toString() {
        return "{" +
                "'id':'" + id + "'," +
                "'chat_name':'" + chat_name + "'," +
                "'id_sender':'" + id_sender + "'," +
                "'id_recipient':'" + id_recipient + "'}";
    }

    public static String toJsonItems(List<Chat> allChats) {
        // Combining result as Json string array
        StringBuilder result = new StringBuilder();
        int count = allChats.size();
        result.append('[');
        for (int i = 0; i < count; i++) {
            result.append(allChats.get(i).toString());
            if ((count > 1) && (i != count - 1))
                result.append(",");
        }
        result.append(']');
        return result.toString();
    }
}