package Ineka.Model;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String surname;

    @Column(unique = true)
    private String email;

    private String password;

    public User() {}

    public User(String name, String surname, String email, String password) {
        this.id = null;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" +
                "'id':'" + id + "'," +
                "'name':'" + name + "'," +
                "'surname':'" + surname + "'," +
                "'email':'" + email + "'," +
                "'password':'" + password + "'}";
    }

    public static String toJsonItems(List<User> allItems) {
        // Combining result as Json string array
        StringBuilder result = new StringBuilder();
        int count = allItems.size();
        result.append('[');
        for (int i = 0; i < count; i++) {
            result.append(allItems.get(i).toString());
            if ((count > 1) && (i != count - 1))
                result.append(",");
        }
        result.append(']');
        return result.toString();
    }
}