package Ineka.Model.Json;

import com.google.common.base.Strings;

public class MessageJson {
    String session;
    String text;
    String chat_id;
    String timestamp;

    public MessageJson(String session, String text, String chat_id, String timestamp) {
        this.session = session;
        this.text = text;
        this.chat_id = chat_id;
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getChatId() {
        return chat_id;
    }

    public void setChatId(String chat_id) {
        this.chat_id = chat_id;
    }

    public Boolean isNotEmpty() {
        return ((!Strings.isNullOrEmpty(getSession())
                && (!Strings.isNullOrEmpty(getChatId()))));
    }

    @Override
    public String toString() {
        return "{\"session\":\"" + session + '\"' +
                ", \"text\":\"" + text + '\"' +
                ", \"chat_id\":\"" + chat_id + '\"' +
                ", \"timestamp\":\"" + timestamp + '\"' +
                '}';
    }
}
