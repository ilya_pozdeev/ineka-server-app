package Ineka.Model.Json;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import com.google.common.base.Strings;

public class RegistrationJson {
    @Email(message = "incorrect email")
    String email;
    @NotEmpty(message = "password cannot be empty")
    String password;
    String name;
    String surname;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public RegistrationJson() {}

    public RegistrationJson(String email, String password, String name, String surname) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }

    public Boolean isNotEmpty() {
        return (!Strings.isNullOrEmpty(getEmail()))
                && (!Strings.isNullOrEmpty(getName()))
                && (!Strings.isNullOrEmpty(getSurname()))
                && (!Strings.isNullOrEmpty(getPassword()));
    }

    @Override
    public String toString() {
        return "{" +
                "\"email\":\"" + email + '\"' +
                ", \"password\":\"" + password + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"surname\":\"" + surname + '\"' +
                '}';
    }
}
