package Ineka.Model.Json;

import com.google.common.base.Strings;

public class SessionJson {
    String session;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public SessionJson() {}

    public SessionJson(String session) {
        this.session = session;
    }

    public Boolean isNotEmpty() {
        return (!Strings.isNullOrEmpty(getSession()));
    }

    @Override
    public String toString() {
        return "{\"session\":\"" + session + "\"}";
    }
}
