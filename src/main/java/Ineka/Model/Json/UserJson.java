package Ineka.Model.Json;

import com.google.common.base.Strings;

public class UserJson {
    String session;
    String name;
    String surname;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public UserJson() {}

    public UserJson(String session, String name, String surname) {
        this.session = session;
        this.name = name;
        this.surname = surname;
    }

    public Boolean isNotEmpty() {

        return (!Strings.isNullOrEmpty(getName()))
                && (!Strings.isNullOrEmpty(getSession()));
    }

    @Override
    public String toString() {
        return "{\"name\":\"" + name + '\"' +
                ", \"surname\":\"" + surname + '\"' +
                ", \"session\":\"" + session + '\"' +
                '}';
    }
}
