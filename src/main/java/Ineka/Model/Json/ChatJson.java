package Ineka.Model.Json;

import com.google.common.base.Strings;

public class ChatJson {
    String session;
    String receiver_email;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getReceiver_email() {
        return receiver_email;
    }

    public void setReceiver_email(String receiver_email) {
        this.receiver_email = receiver_email;
    }

    public ChatJson() {}

    public ChatJson(String session, String receiver_email) {
        this.session = session;
        this.receiver_email = receiver_email;
    }

    public Boolean isNotEmpty() {
        return (!Strings.isNullOrEmpty(getSession()))
                && (!Strings.isNullOrEmpty(getReceiver_email()));
    }

    @Override
    public String toString() {
        return "{\"session\":\"" + session + '\"' +
                ", \"receiver_email\":\"" + receiver_email + '\"' +
                '}';
    }
}
