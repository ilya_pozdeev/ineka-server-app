package Ineka.Model.Json;

import com.google.common.base.Strings;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class LoginJson {
    String email;
    String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginJson() {}

    public LoginJson(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public Boolean isNotEmpty() {

        return (!Strings.isNullOrEmpty(getEmail()))
                && (!Strings.isNullOrEmpty(getPassword()));
    }

    @Override
    public String toString() {
        return "{\"email\":\"" + email + '\"' +
                ", \"password\":\"" + password + '\"' +
                '}';
    }
}
