package Ineka.Model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Integer> {
    @Query("FROM User WHERE email = ?1")
    User findByEmail(String email);

    @Query("FROM User WHERE name like %?1%")
    ArrayList<User> findByName(String name);

    @Query("FROM User WHERE name like %?1% and surname like %?2%")
    ArrayList<User> findByNameSurname(String name, String surname);
}