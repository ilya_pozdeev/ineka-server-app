package Ineka.Model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called chatRepository
// CRUD refers Create, Read, Update, Delete

public interface ChatRepository extends CrudRepository<Chat, Integer> {

    @Query("FROM Chat WHERE chat_name = ?1")
    ArrayList<Chat> findByChatName(String chatName);

    @Query("FROM Chat WHERE id IN (:ids)")
    ArrayList<Chat> findByIds(@Param("ids") List<Integer> ids);

    @Query("FROM Chat WHERE (id_sender = ?1 or id_recipient = ?1)")
    ArrayList<Chat> findByUserId(Integer id);

    @Query("FROM Chat WHERE ((id_sender = ?1 or id_sender = ?1) and (id_recipient = ?2 or id_recipient = ?2))")
    Chat findByUsersIds(Integer id1, Integer id2);
}