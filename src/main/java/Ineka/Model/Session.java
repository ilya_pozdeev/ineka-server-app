package Ineka.Model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Session {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String value;

    private Integer id_user;

    private Timestamp date_time;

    public Session() {
        this.date_time = new Timestamp(System.currentTimeMillis());
    }

    public Session(String value, Integer id_user) {
        this.id = null;
        this.value = value;
        this.id_user = id_user;
        this.date_time = new Timestamp(System.currentTimeMillis());
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getIdUser() {
        return id_user;
    }

    public void setIdUser(Integer id_user) {
        this.id_user = id_user;
    }

    public Timestamp getDateTime() {
        return date_time;
    }

    public void setDateTime(Timestamp date_time) {
        this.date_time = date_time;
    }

    @Override
    public String toString() {
        return "{'id':'" + id + "'," +
                "'value':'" + value + "'," +
                "'date_time':'" + date_time + "'," +
                "'id_user':'" + id_user + "'}";
    }
}