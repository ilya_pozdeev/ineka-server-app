package Ineka.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    private Timestamp date_time;

    private String text;

    private String path_to_file;

    private Integer id_chat;

    private Integer id_sender;

    public Message() {
        this.date_time = new Timestamp(System.currentTimeMillis());
    }

    public Message(String text, String path_to_file, Integer id_chat, Integer id_sender) {
        this.id = null;
        this.date_time = new Timestamp(System.currentTimeMillis());
        this.text = text;
        this.path_to_file = path_to_file;
        this.id_chat = id_chat;
        this.id_sender = id_sender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDateTime() {
        return date_time;
    }

    public void setDateTime(Timestamp date_time) {
        this.date_time = date_time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPathToFile() {
        return path_to_file;
    }

    public void setPathToFile(String path_to_file) {
        this.path_to_file = path_to_file;
    }

    public Integer getIdChat() {
        return id_chat;
    }

    public void setIdChat(Integer id_chat) {
        this.id_chat = id_chat;
    }

    public Integer getIdSender() {
        return id_sender;
    }

    public void setIdSender(Integer id_sender) {
        this.id_sender = id_sender;
    }

    public static String toJsonItems(List<Message> allItems) {
        // Combining result as Json string array
        StringBuilder result = new StringBuilder();
        int count = allItems.size();
        result.append('[');
        for (int i = 0; i < count; i++) {
            result.append(allItems.get(i).toString());
            if ((count > 1) && (i != count - 1))
                result.append(",");
        }
        result.append(']');
        return result.toString();
    }

    @Override
    public String toString() {
        return "{" +
                "'id':'" + id + "'," +
                "'date_time':'" + date_time + "'," +
                "'text':'" + text + "'," +
                "'path_to_file':'" + path_to_file + "'," +
                "'id_chat':'" + id_chat + "'," +
                "'id_sender':'" + id_sender + "'}";
    }
}