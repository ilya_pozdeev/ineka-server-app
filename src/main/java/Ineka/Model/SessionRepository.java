package Ineka.Model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface SessionRepository extends CrudRepository<Session, Integer> {
    @Query("FROM Session WHERE id_user = ?1")
    ArrayList<Session> findByUserId(Integer id);

    @Query("FROM Session WHERE value = ?1")
    Session findByValue(String value);
}