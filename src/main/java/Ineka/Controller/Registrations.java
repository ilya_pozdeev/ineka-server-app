package Ineka.Controller;

import Ineka.Model.*;
import Ineka.Model.Json.LoginJson;
import Ineka.Model.Json.RegistrationJson;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;

import org.apache.commons.text.RandomStringGenerator;

import java.util.ArrayList;

@RestController
public class Registrations {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @PostMapping(path = "/registration")
    public @ResponseBody String register(@RequestBody RegistrationJson json) {
        JSONObject jo = new JSONObject();

        String newSession = "";

        if (json.isNotEmpty()) {
            try {
                User newUser = null;
                try {
                    newUser = userRepository.save(new User(json.getName(), json.getSurname(), json.getEmail(), json.getPassword()));
                } catch (DataIntegrityViolationException e) {
                    jo.put("session", "");
                    jo.put("message", "Error: Email is already used !");
                    return jo.toString();
                }
                short attempts = 100;
                while(attempts > 0) {
                    attempts--;
                    try {
                        newSession = generateSessionKey();
                        sessionRepository.save(new Session(newSession, newUser.getId()));
                        break;
                    } catch (DataIntegrityViolationException ignored) {}
                }
                Assert.isTrue(attempts > 0);
                jo.put("session", newSession);
                jo.put("message", "Success");
            } catch (IllegalArgumentException e) {
                jo.put("session", "");
                jo.put("message", "Error: Something went wrong during saving new session (db error)");
            } catch (Throwable e) {
                jo.put("session", "");
                jo.put("message", "Error: Something went wrong during creating elements (db error)");
            }
        } else {
            jo.put("session", "");
            jo.put("message", "Error: Some fields are not set properly (empty)");
        }

        return jo.toString();
    }

    @PostMapping(path = "/login")
    public @ResponseBody String register(@RequestBody LoginJson json) {
        JSONObject jo = new JSONObject();
        String newSession = "";

        if (json.isNotEmpty()) {
            try {
                User user = userRepository.findByEmail(json.getEmail());
                if (user == null) {
                    jo.put("session", "");
                    jo.put("message", "Error: No such user in database");
                    return jo.toString();
                }
                if (!user.getPassword().equals(json.getPassword())) {
                    jo.put("session", "");
                    jo.put("message", "Error: Password not correct");
                    return jo.toString();
                }
                ArrayList<Session> userSessions = sessionRepository.findByUserId(user.getId());
                if (userSessions.size() > 0)
                    sessionRepository.delete(userSessions.get(0));

                short attempts = 100;
                while(attempts > 0) {
                    attempts--;
                    try {
                        newSession = generateSessionKey();
                        sessionRepository.save(new Session(newSession, user.getId()));
                        break;
                    } catch (DataIntegrityViolationException ignored) {}
                }
                Assert.isTrue(attempts > 0);
                jo.put("session", newSession);
                jo.put("message", "Success");
            } catch (IllegalArgumentException e) {
                jo.put("session", "");
                jo.put("message", "Error: Something went wrong during saving new session (db error)");
            } catch (Throwable e) {
                jo.put("session", "");
                jo.put("message", "Error: No such user in database");
            }
        } else {
            jo.put("session", "");
            jo.put("message", "Error: Some fields are not set properly (empty)");
        }

        return jo.toString();
    }

    protected String generateSessionKey() {
        final String charsForNewSession = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcd%efgh^ijkl@mno#pqrs*tuvwxy&z0123456789";

        RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder()
                .selectFrom(charsForNewSession.toCharArray())
                .build();

        return randomStringGenerator.generate(25);
    }
}
