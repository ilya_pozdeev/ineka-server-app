package Ineka.Controller;

import Ineka.Model.Json.UserJson;
import Ineka.Model.Session;
import Ineka.Model.SessionRepository;
import Ineka.Model.User;
import Ineka.Model.UserRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class Users {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @PostMapping("/user/find")
    public @ResponseBody String getUser(@RequestBody UserJson json) {
        JSONObject jo = new JSONObject();
        if (json.isNotEmpty()) {
            Session foundSession = sessionRepository.findByValue(json.getSession());

            if (foundSession != null) {
                ArrayList<User> foundUsers;
                if ("".equals(json.getSurname()) || (json.getSurname() == null))
                    foundUsers = userRepository.findByName(json.getName());
                else
                    foundUsers = userRepository.findByNameSurname(json.getName(), json.getSurname());

                jo.put("result", User.toJsonItems(foundUsers));
                jo.put("message", "Found " + foundUsers.size() + " users");
                return jo.toString();
            }
            jo.put("result", "");
            jo.put("message", "Error: Session is not found");
            return jo.toString();
        }
        jo.put("result", "");
        jo.put("message", "Error: Some fields are not set properly (empty)");
        return jo.toString();
    }
}
