package Ineka.Controller;

import org.springframework.web.bind.annotation.*;

@RestController // RestController is better solution for json
public class Main {
    @PostMapping("/")
    public String test() {
        return "Hello from Ineka server";
    }

    @GetMapping("/")
    public String hello() { return "Hello from Ineka server"; }
}