package Ineka.Controller;

import Ineka.Model.*;
import Ineka.Model.Json.ChatJson;
import Ineka.Model.Json.SessionJson;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
public class Chats {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private ChatRepository chatRepository;

    @PostMapping("/get/my/chats")
    public @ResponseBody String getAllMyChatsExample(@RequestBody SessionJson json) {
        JSONObject jo = new JSONObject();
        Session foundSession = sessionRepository.findByValue(json.getSession());

        if (foundSession != null) {
            Session session = sessionRepository.findByValue(json.getSession());
            ArrayList<Chat> foundChat = chatRepository.findByUserId(session.getIdUser());

            jo.put("result", Chat.toJsonItems(foundChat));
            jo.put("message", "Found " + foundChat.size() + " chats");
            return jo.toString();
        }
        jo.put("result", "");
        jo.put("message", "Error: Session is not found");
        return jo.toString();
    }

    @PostMapping("/new/chat")
    public @ResponseBody String createNewChat(@RequestBody ChatJson json) {
        JSONObject jo = new JSONObject();

        if (json.isNotEmpty()) {
            Session foundSession = sessionRepository.findByValue(json.getSession());

            if (foundSession != null) {
                User currentUser;
                User userToChatWith;

                try {
                    // Chat creator information
                    Session currentSession = sessionRepository.findByValue(json.getSession());
                    currentUser = userRepository.findById(currentSession.getIdUser()).orElse(null);

                    // Receiver information
                    userToChatWith = userRepository.findByEmail(json.getReceiver_email());
                } catch (Throwable e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting users information (db error)");
                    return jo.toString();
                }

                if ((currentUser == null) || (userToChatWith == null)) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting users information (not correct data)");
                    return jo.toString();
                }
                Chat existedChat = chatRepository.findByUsersIds(currentUser.getId(), userToChatWith.getId());

                if (existedChat != null) {
                    jo.put("result", existedChat.toString());
                    jo.put("message", "Warning: One chat already exists. It was returned as result.");
                    return jo.toString();
                }

                Chat newChat;

                try {
                    newChat = chatRepository.save(new Chat(currentUser.getName() + " - " + userToChatWith.getName(), currentUser.getId(), userToChatWith.getId()));
                } catch (Throwable e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during creating new chat (db error)");
                    return jo.toString();
                }

                jo.put("result", newChat.toString());
                jo.put("message", "Chat: " + newChat.getChatName() + " is created");
                return jo.toString();
            }
            jo.put("result", "");
            jo.put("message", "Error: Session is not found");
            return jo.toString();
        }
        jo.put("result", "");
        jo.put("message", "Error: Some fields are not set properly (empty)");
        return jo.toString();
    }
}
