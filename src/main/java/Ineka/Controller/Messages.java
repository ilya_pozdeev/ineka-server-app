package Ineka.Controller;

import Ineka.Model.*;
import Ineka.Model.Json.*;
import io.jsonwebtoken.lang.Assert;
import org.apache.commons.text.RandomStringGenerator;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
public class Messages {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private MessageRepository messageRepository;

    @PostMapping("/send/message")
    public @ResponseBody String sendMessage(@RequestBody MessageJson json) {
        JSONObject jo = new JSONObject();

        if (json.isNotEmpty()) {
            Session foundSession = sessionRepository.findByValue(json.getSession());

            if (foundSession != null) {
                User currentUser;
                Chat foundChat;

                try {
                    // Chat creator information
                    Session currentSession = sessionRepository.findByValue(json.getSession());
                    currentUser = userRepository.findById(currentSession.getIdUser()).orElse(null);

                    // Receiver information
                    foundChat = chatRepository.findById(Integer.parseInt(json.getChatId())).orElse(null);
                } catch (Throwable e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting users information (db error)");
                    return jo.toString();
                }

                if (currentUser == null) {
                    jo.put("result", "");
                    jo.put("message", "Error: Sender data was not found in database (db error)");
                    return jo.toString();
                }

                if (foundChat == null) {
                    jo.put("result", "");
                    jo.put("message", "Error: Chat was not found in database (db error)");
                    return jo.toString();
                }

                if ((!foundChat.getIdRecipient().equals(currentUser.getId()))
                        && (!foundChat.getIdSender().equals(currentUser.getId()))) {
                    jo.put("result", "");
                    jo.put("message", "Error: This user is not participating in chat (access denied)");
                    return jo.toString();
                }

                Message savedMessage;
                try {
                    savedMessage = messageRepository.save(new Message(json.getText(), "", foundChat.getId(), currentUser.getId()));
                } catch (Exception e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during saving new message in database (db error)");
                    return jo.toString();
                }

                jo.put("result", savedMessage.toString());
                jo.put("message", "Message saved for " + foundChat.getChatName() + " chat");
                return jo.toString();
            }
            jo.put("result", "");
            jo.put("message", "Error: Session is not found");
            return jo.toString();
        }
        jo.put("result", "");
        jo.put("message", "Error: Some fields are not set properly (empty)");
        return jo.toString();
    }

    @PostMapping("/get/chat/messages")
    public @ResponseBody String getAllChatMessages(@RequestBody MessageJson json) {
        JSONObject jo = new JSONObject();

        if (json.isNotEmpty()) {
            Session foundSession = sessionRepository.findByValue(json.getSession());

            if (foundSession != null) {
                User currentUser;
                User userToChatWith;
                Chat foundChat;
                try {
                    // Chat creator information
                    Session currentSession = sessionRepository.findByValue(json.getSession());
                    currentUser = userRepository.findById(currentSession.getIdUser()).orElse(null);

                    // Receiver information
                    foundChat = chatRepository.findById(Integer.parseInt(json.getChatId())).orElse(null);
                    int userToChatWithId = foundChat.getIdSender().equals(currentUser.getId()) ? foundChat.getIdRecipient() : currentUser.getId();
                    userToChatWith = userRepository.findById(userToChatWithId).orElse(null);
                } catch (Throwable e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting users information (db error)");
                    return jo.toString();
                }

                if (userToChatWith == null) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting other user information (db error)");
                    return jo.toString();
                }

                List<Message> chatMessages = messageRepository.findAllByChatId(foundChat.getId());

                if (chatMessages == null) {
                    jo.put("result", "");
                    jo.put("message", "Chat has no messages");
                    return jo.toString();
                }

                // Sorting messages by date - ASC
                chatMessages.sort(Comparator.comparing(Message::getDateTime));

                jo.put("result", Message.toJsonItems(chatMessages));
                jo.put("message", "Found: " + chatMessages.size() + " messages for " + foundChat.getChatName() + " chat");
                return jo.toString();
            }
            jo.put("result", "");
            jo.put("message", "Error: Session is not found");
            return jo.toString();
        }
        jo.put("result", "");
        jo.put("message", "Error: Some fields are not set properly (empty)");
        return jo.toString();
    }

    @PostMapping("/get/chat/new-messages")
    public @ResponseBody String getAllNewChatMessages(@RequestBody MessageJson json) {
        JSONObject jo = new JSONObject();

        if (json.isNotEmpty()) {
            Session foundSession = sessionRepository.findByValue(json.getSession());

            if (foundSession != null) {
                if (json.getTimestamp() == null) {
                    jo.put("result", "");
                    jo.put("message", "Error: Timestamp is not set (empty)");
                    return jo.toString();
                }
                Timestamp time;
                try {
                    time = Timestamp.valueOf(json.getTimestamp());
                } catch (Exception e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Timestamp string format is not accepted (wrong)");
                    return jo.toString();
                }

                User currentUser;
                User userToChatWith;
                Chat foundChat;
                int userToChatWithId;
                try {
                    // Chat creator information
                    Session currentSession = sessionRepository.findByValue(json.getSession());
                    currentUser = userRepository.findById(currentSession.getIdUser()).orElse(null);

                    // Receiver information
                    foundChat = chatRepository.findById(Integer.parseInt(json.getChatId())).orElse(null);
                    if (foundChat.getIdSender() == currentUser.getId())
                        userToChatWith = userRepository.findById(foundChat.getIdRecipient()).orElse(null);
                    else
                        userToChatWith = userRepository.findById(foundChat.getIdSender()).orElse(null);
                } catch (Throwable e) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting users information (db error)");
                    return jo.toString();
                }

                if (userToChatWith == null) {
                    jo.put("result", "");
                    jo.put("message", "Error: Something went wrong during getting other user information (db error)");
                    return jo.toString();
                }

                List<Message> chatMessages = messageRepository.findAllByChatId(foundChat.getId());

                if (chatMessages == null) {
                    jo.put("result", "");
                    jo.put("message", "Chat has no messages");
                    return jo.toString();
                }

                // Sorting messages by date - ASC
                chatMessages.sort(Comparator.comparing(Message::getDateTime));

                ArrayList<Message> newMessagesForChat = new ArrayList<>();

                // Getting only new messages for current user
                userToChatWithId = userToChatWith.getId();
                for (Message message : chatMessages) {
                    if ((time.compareTo(message.getDateTime()) < 0)
                    && (message.getIdSender().equals(userToChatWithId)))
                        newMessagesForChat.add(message);
                }

                jo.put("result", Message.toJsonItems(newMessagesForChat));
                jo.put("message", "Found: " + newMessagesForChat.size() + " messages for " + foundChat.getChatName() + " chat");
                return jo.toString();
            }
            jo.put("result", "");
            jo.put("message", "Error: Session is not found");
            return jo.toString();
        }
        jo.put("result", "");
        jo.put("message", "Error: Some fields are not set properly (empty)");
        return jo.toString();
    }
}
